package com.hendisantika.springboothystrix;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hystrix
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/08/18
 * Time: 20.26
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/demo")
public class HystrixController {
    @HystrixCommand(fallbackMethod = "fallbackHello", commandKey = "hello", groupKey = "hello")
    @GetMapping("/hello")
    public String hello() {
        if (RandomUtils.nextBoolean()) {
            throw new RuntimeException("Failed!");
        }
        return "Hello World from hello method";
    }

    @HystrixCommand(fallbackMethod = "fallbackHello", commandKey = "helloAnother", groupKey = "helloAnother")
    @GetMapping("/helloAnother")
    public String helloAnother() {
        if (RandomUtils.nextBoolean()) {
            throw new RuntimeException("Failed!");
        }
        return "Hello World from helloAnother method";
    }

    public String fallbackHello() {
        return "FallBack Hello Initiated";
    }
}
